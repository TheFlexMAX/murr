from django.urls import path

from murr_websocket.consumers.murr_battle_room import MurrBattleRoomConsumer
from murr_websocket.consumers.murr_chat import MurrChatConsumer
from murr_websocket.consumers.murr_service import MurrServiceConsumer


websocket_urls = [
    path('ws/murr_chat/<str:murr_ws_name>/', MurrChatConsumer),
    path('ws/murr_battle_room/<str:murr_ws_name>/', MurrBattleRoomConsumer),
    path('ws/service/<str:murr_ws_name>/', MurrServiceConsumer),
]
