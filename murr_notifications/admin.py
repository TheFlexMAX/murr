from django.contrib import admin

from murr_notifications.models import FeedNotification, SubscribeNotification


@admin.register(FeedNotification)
class FeedNotificationAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'sent_time',)
    exclude = ('notification_type', )


@admin.register(SubscribeNotification)
class SubscribeNotificationAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'sent_time',)
    exclude = ('notification_type', )
