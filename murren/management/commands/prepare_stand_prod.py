from django.core.management import BaseCommand
from django.contrib.auth import get_user_model

from murr_card.models import Category
from murr_websocket.models import MurrWebSocket, MurrWebSocketType

Murren = get_user_model()


class Command(BaseCommand):
    help = 'Подготовить свежий стенд / Prepare fresh stand'
    CATEGORIES = {'Аниме': 'anime', 'Кодинг': 'coding', 'Игры': 'game', 'Other': 'other'}

    def handle(self, *args, **options):
        for category in self.CATEGORIES:
            Category.objects.create(name=category, slug=self.CATEGORIES[category])
            print(f'Создана категория {category}')

        murr_tavern = MurrWebSocket.objects.create(murr_ws_name='murr_tavern',
                                                   murr_ws_type=MurrWebSocketType.MURR_CHAT)
        if murr_tavern:
            print(
                f"Мурр таверна создана - link: {murr_tavern.link}, "
                f"pk: {murr_tavern.id}, "
                f"murr_ws_name: {murr_tavern.murr_ws_name}"
                f"murr_ws_type: {murr_tavern.murr_ws_type}"
            )

        notifications = MurrWebSocket.objects.create(murr_ws_name='notifications',
                                                     murr_ws_type=MurrWebSocketType.SERVICE)
        if notifications:
            print(
                f"Соккет для уведомлений - link: {notifications.link}, "
                f"pk: {notifications.id}, "
                f"murr_ws_name: {notifications.murr_ws_name}"
                f"murr_ws_type: {notifications.murr_ws_type}"
            )
