from django.core.management import BaseCommand
from django.contrib.auth import get_user_model

from murr_card.models import Category, MurrCard, MurrCardStatus
from murr_websocket.models import MurrWebSocket, MurrWebSocketType

Murren = get_user_model()


class Command(BaseCommand):
    help = 'Подготовить свежий стенд для разработки / Prepare fresh stand for develop'
    CATEGORIES = {'Аниме': 'anime', 'Кодинг': 'coding', 'Игры': 'game', 'Other': 'other'}
    TESTED_MURRENS_NAME = ['Greg', 'Moro', 'TREMOR']

    def handle(self, *args, **options):
        admin = Murren.objects.create_superuser('admin', 'admin@admin.com', 'admin')
        if admin:
            print("Администратор создан успешно - креды - admin/admin")

        for murren in self.TESTED_MURRENS_NAME:
            tested_murren = Murren.objects.create_user(murren, f'{murren}@yandex.com', '1q2w3e!')
            if tested_murren:
                print(f"Муррен {murren} создан - креды - {murren}/1q2w3e!")

        for category in self.CATEGORIES:
            Category.objects.create(name=category, slug=self.CATEGORIES[category])
            print(f'Создана категория {category}')

        murr = MurrCard.objects.create(
            title='Это тестовый мурр. Он был создан при вызове команды prepare_stand_dev. '
                  'Желаю удачной разработки и крепкого  здоровья',
            content='{"blocks":[{"type":"header","data":{"text":"Мурр - карточка с информацией в Мурренган.",'
                    '"level":2}},{"type":"paragraph","data":{"text":"Обязательной частью мурра является заголовок - '
                    'строка длиной до 224 символов. Заголовок описывает суть мурра."}},{"type":"paragraph","data":'
                    '{"text":"Мурр может иметь обложку (murr_cover - мурр_кавер). Она отображается вместе с заголовком'
                    ' в ленте мурров. Обложка имеет произвольно прямоугольный формат."}},{"type":"paragraph","data":'
                    '{"text":"Внутри мурра можно писать текст - этот абзац - это текст."}},{"type":"header","data":'
                    '{"text":"Можно делать заголовоки - вот пример","level":2}},{"type":"paragraph","data":{"text":'
                    '"Прикрепи внутрь мурра картинку вот так:"}},{"type":'
                    '"image","data":{"file":{"url":"http://127.0.0.1:8000/'
                    'media/default_murren_avatar.png"},"caption":"секси лейди","withBorder":false,"stretched":false,'
                    '"withBackground":false}},{"type":"header","data":{"text":"Обязательно поделись своим мурром! '
                    'Скопируй ссылку нажав на ... когда сохранишь мурр!","level":2}},{"type":"paragraph","data":'
                    '{"text":"И да пребудет с тобой сила!"}},{"type":"paragraph","data":{"text":"Мурренган"}}]}',
            owner=tested_murren, cover='tanochka.jpg', status=MurrCardStatus.RELEASE)
        if murr:
            print("Тестовый мурр создан")

        murr_tavern = MurrWebSocket.objects.create(murr_ws_name='murr_tavern',
                                                   murr_ws_type=MurrWebSocketType.MURR_CHAT)
        if murr_tavern:
            print(
                f"Мурр таверна создана - link: {murr_tavern.link}, "
                f"pk: {murr_tavern.id}, "
                f"murr_ws_name: {murr_tavern.murr_ws_name}"
                f"murr_ws_type: {murr_tavern.murr_ws_type}"
            )
        lobby = MurrWebSocket.objects.create(murr_ws_name='lobby',
                                             murr_ws_type=MurrWebSocketType.MURR_BATTLE_ROOM)
        if lobby:
            print(
                f"Лобби для мурр гейм создано - link: {lobby.link}, "
                f"pk: {lobby.id}, "
                f"murr_ws_name: {lobby.murr_ws_name}"
                f"murr_ws_type: {lobby.murr_ws_type}"
            )

        notifications = MurrWebSocket.objects.create(
            murr_ws_name='notifications',
            murr_ws_type=MurrWebSocketType.SERVICE
        )
        if notifications:
            print(
                f"Соккет для уведомлений - link: {notifications.link}, "
                f"pk: {notifications.id}, "
                f"murr_ws_name: {notifications.murr_ws_name}"
                f"murr_ws_type: {notifications.murr_ws_type}"
            )
