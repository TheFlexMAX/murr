import pytest
from django.urls import reverse
from rest_framework import status


pytestmark = [
    pytest.mark.django_db,
]


def test_like_murr_card(create_release_murr, api_client):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    api_client.force_authenticate(user=murren)

    url = reverse('murr_card-like', kwargs={'pk': murr.id})

    response_like = api_client.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 1
    assert murr.rating == 1
    assert murr.liked_murrens.first() == murren
    assert response_like.data['is_like']
    assert not response_like.data['is_dislike']

    response_like = api_client.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr.rating == 0
    assert murr.liked_murrens.first() is None
    assert not response_like.data['is_like']
    assert not response_like.data['is_dislike']


def test_like_murr_card_not_authenticated(create_release_murr, api_client):
    murr = create_release_murr['murr_card']

    url = reverse('murr_card-like', kwargs={'pk': murr.id})
    response = api_client.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_like_murr_card_by_baned_murr(create_murren_is_banned, create_release_murr, api_client):
    murren = create_murren_is_banned
    murr = create_release_murr['murr_card']

    api_client.force_authenticate(user=murren)

    url = reverse('murr_card-like', kwargs={'pk': murr.id})

    response_like = api_client.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr.rating == 0
    assert murr.liked_murrens.first() is None
    assert not response_like.data['is_like']
    assert not response_like.data['is_dislike']


def test_dislike_murr_card(create_release_murr, api_client):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    api_client.force_authenticate(user=murren)

    url = reverse('murr_card-dislike', kwargs={'pk': murr.id})

    response_like = api_client.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == -1
    assert murr.rating == -1
    assert murr.disliked_murrens.first() == murren
    assert not response_like.data['is_like']
    assert response_like.data['is_dislike']

    response_like = api_client.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr.rating == 0
    assert murr.disliked_murrens.first() is None
    assert not response_like.data['is_like']
    assert not response_like.data['is_dislike']


def test_dislike_murr_card_not_authenticated(create_release_murr, api_client):
    murr = create_release_murr['murr_card']

    url = reverse('murr_card-dislike', kwargs={'pk': murr.id})
    response = api_client.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_dislike_murr_card_by_baned_murr(create_murren_is_banned, create_release_murr, api_client):
    murren = create_murren_is_banned
    murr = create_release_murr['murr_card']

    api_client.force_authenticate(user=murren)

    url = reverse('murr_card-dislike', kwargs={'pk': murr.id})

    response_like = api_client.post(url)
    murr.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr.rating == 0
    assert murr.liked_murrens.first() is None
    assert not response_like.data['is_like']
    assert not response_like.data['is_dislike']


def test_like_murr_comment(create_murr_comment, api_client):
    murren = create_murr_comment['murren']
    murr_comment = create_murr_comment['murr_comment']
    api_client.force_authenticate(user=murren)

    url = reverse('comment-like', kwargs={'pk': murr_comment.id})

    response_like = api_client.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 1
    assert murr_comment.rating == 1
    assert murr_comment.liked_murrens.first() == murren

    response_like = api_client.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr_comment.rating == 0
    assert murr_comment.liked_murrens.first() is None


def test_like_murr_comment_not_authenticated(create_murr_comment, api_client):
    murr_comment = create_murr_comment['murr_comment']

    url = reverse('comment-like', kwargs={'pk': murr_comment.id})
    response = api_client.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_dislike_murr_comment(create_murr_comment, api_client):
    murren = create_murr_comment['murren']
    murr_comment = create_murr_comment['murr_comment']
    api_client.force_authenticate(user=murren)

    url = reverse('comment-dislike', kwargs={'pk': murr_comment.id})

    response_like = api_client.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == -1
    assert murr_comment.rating == -1
    assert murr_comment.disliked_murrens.first() == murren

    response_like = api_client.post(url)
    murr_comment.refresh_from_db()
    assert response_like.status_code == status.HTTP_200_OK
    assert response_like.data.get('rating') == 0
    assert murr_comment.rating == 0
    assert murr_comment.disliked_murrens.first() is None


def test_dislike_murr_comment_not_authenticated(create_murr_comment, api_client):
    murr_comment = create_murr_comment['murr_comment']

    url = reverse('comment-like', kwargs={'pk': murr_comment.id})
    response = api_client.post(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
