export default {
  render() {
    return this.$slots.default[0];
  },

  props: {
    minHeight: {
      type: Number,
      required: false,
      default: null,
    },
  },

  data() {
    return {
      defaultHeight: "auto",
    };
  },

  mounted() {
    this.$nextTick(() => {
      let height = this.$el.offsetHeight;

      if (this.minHeight) {
        height = this.minHeight;
      }

      this.defaultHeight = height + "px";
      this.setHeightAttribute(this.defaultHeight);
    });

    this.$el.addEventListener("input", this.handleInput);
    this.$el.addEventListener("blur", this.handleBlur);
  },

  methods: {
    setHeightAttribute(height) {
      this.$nextTick(() => {
        this.$el.style.height = height;
        this.$el.style.overflow = "hidden";
      });
    },

    handleInput({ target }) {
      if (target.value.length > 0) {
        target.style.height = "auto";
        target.style.height = target.scrollHeight + "px";
      } else {
        this.setHeightAttribute(this.defaultHeight);
      }
    },

    handleBlur(event) {
      if (event.target.value.length === 0) {
        this.setHeightAttribute(this.defaultHeight);
      }
    },
  },

  updated() {
    if (this.$el.value.length === 0) {
      this.setHeightAttribute(this.defaultHeight);
    }
  },

  beforeDestroy() {
    this.$el.removeEventListener("input", this.handleInput);
    this.$el.removeEventListener("blur", this.handleBlur);
  },
};
